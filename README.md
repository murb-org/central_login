# CentralLogin - An OpenID Connect Provider

This app builds on the foundations of the [Doorkeeper](https://github.com/doorkeeper-gem/doorkeeper), [Doorkeeper::OpenidConnect](https://github.com/doorkeeper-gem/doorkeeper-openid_connect) and [Devise](https://github.com/heartcombo/devise) to provide a central login system.

While Doorkeeper supports other OAuth flows, CentralLogin focusses on OpenID Connect as it is a more complete, and hence useful standard, for most use cases. This project builds on years of juggling with different authentication providers and implementations. It may cut corners to be a pragmatic and less flexible solution. But it is open source, you can adjust it on your own. And it also allows you to host it on your own. You don't have to tie your users to a closed authentication system such as Auth0, Azure Directory, Cognito (the horror) or something else. In the past I've been a happy user of [Keycloak](https://www.keycloak.org/), which is definitely way more advanced than this project, but it in the end it is a Java application and hence harder for me to maintain and not focussed on what I think are the core requirements :)

## Applications

### User authentication

Steps:

1. User selects CentralLogin as the identity provider
2. User logs in at CentralLogin
3. User is redirected back to the application with a callback
4. The application verifies the callback token against the .well-known data of the trusted CentralLogin instance
5. The application can now perform task on behalve of the user

### Machine authentication

Steps:

1. The machine makes a grant_type=client_credentials post to the /oauth/token end-point (e.g. `curl -X POST -d 'grant_type=client_credentials' -d 'client_id=...' -d 'client_secret=...' https://central_login_domain/oauth/token --verbose`)
2. The receiving machine can now use this token to authenticate with other machines
3. Other machines verify the token against the .well-known data of the trusted CentralLogin instance.

### User Authorization

Users can be authorized using roles and groups; which can be assigned to users in groups. These roles and groups are exposed in respectively the roles and groups claims.

## Near future

* 2FA authentication; a HMAC generating gem has already been added but still has to be made to work...
* Support theming (using the [BrandingRepo](https://murb.nl/blog?tags=branding_repo)-gem)
* Fully translatable (adding support for Dutch & English)
* More testing (note: the heavy lifting is done by the above mentioned gems, not this tool)

## Development

The application is a standard Rails. It should behave as such. `gem install` and the required dependencies should be there.

## Related plugins

If you want to use OpenID Connect with Rails & Devise, I might just have the right plugin for you **TODO**

## Use in the real world

I'll be using this for a few of my clients in production.

## Contribution

Feel free to contribute.