unless Rails.env.development?
  Rails.application.config.middleware.use ExceptionNotification::Rack,
    email: {
      email_prefix: "[CentralLogin #{Rails.env}] ",
      sender_address: %("notifier" <notifier@murb.nl>),
      exception_recipients: %w[central_login@murb.nl]
    }
end
