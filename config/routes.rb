Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: "users/registrations"
  }
  resources :groups
  resources :users, only: [:index, :edit, :update, :destroy] do
    post :reset_password
    post :reset_confirmation
    resources :events, only: [:index]
  end
  resources :events, only: [:index]
  use_doorkeeper_openid_connect
  use_doorkeeper

  root to: "application#home"
end
