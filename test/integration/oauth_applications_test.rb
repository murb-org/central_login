require "test_helper"

class OauthApplicationsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_login
    # get the login page
    get "/"
    assert_redirected_to "/users/sign_in"
    follow_redirect!
    assert_equal 200, status

    # post the login and follow through to the home page
    sign_in users(:one)

    get "/"
    assert_match("Uitloggen", response.body)

    get "/oauth/applications"
    follow_redirect!
    assert_match("You&#39;re not allowed to manage applications", response.body)
  end

  def test_admin_behaviour
    sign_in users(:admin)

    get "/oauth/applications"
    assert_match("Jouw applicaties", response.body)
  end
end
