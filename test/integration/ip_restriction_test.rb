require "test_helper"

class OauthApplicationsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_block_login
    # get the login page
    get "/"
    assert_redirected_to "/users/sign_in"
    follow_redirect!
    assert_equal 200, status

    user = users(:one)
    user.update(ip_restriction: "12.12.12.12")

    # post the login and follow through to the home page
    sign_in users(:one)

    get "/"
    assert_no_match("Uitloggen", response.body)
  end

  def test_success_login
    # get the login page
    get "/"
    assert_redirected_to "/users/sign_in"
    follow_redirect!
    assert_equal 200, status

    user = users(:one)
    user.update(ip_restriction: "127.0.0.1,12.12.12.12")

    # post the login and follow through to the home page
    sign_in users(:one)

    get "/"
    assert_match("Uitloggen", response.body)
  end
end
