require "simplecov"
SimpleCov.start "rails" do
  add_filter %r{^/test/}
  add_filter %r{^/vendor/}
  add_filter %r{^/import/}
end

ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  parallelize_setup do |worker|
    SimpleCov.command_name "#{SimpleCov.command_name}-#{worker}"
  end

  parallelize_teardown do |_worker|
    SimpleCov.result
  end

  # Add more helper methods to be used by all tests here...

  def inspect
    # prevents logging enormous inspect blobs
    "<#{self.class.name}>"
  end
end
