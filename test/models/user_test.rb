require "test_helper"

# noinspection RubyNilAnalysis
class UserTest < ActiveSupport::TestCase
  test "gets assigned a uuid" do
    u = User.new(email: "a@murb.nl", password: "asdfasdf", password_confirmation: "asdfasdf")
    u.save
    assert(u.uuid)
  end

  test "#ip_restriction ip restriction is empty by default" do
    u = users(:one)
    assert_equal([], u.ip_restriction)
  end

  test "#ip_restriction ip restriction is inherited from groups" do
    restriction = %w[1.2.3.4 2.3.4.5]

    g = Group.create(name: "test", ip_restriction: restriction)
    u = users(:one)
    u.groups << g
    assert_equal(restriction, u.ip_restriction)
  end

  test "#adheres_to_ip_restriction?" do
    u = users(:one)
    u.update(ip_restriction: %w[1.2.3.4])
    assert(u.adheres_to_ip_restriction?("1.2.3.4"))
    assert_not(u.adheres_to_ip_restriction?("2.3.4.5"))
  end

  test "#ip_restriction= accepts a string" do
    restriction = %w[1.2.3.4 2.3.4.5]
    u = users(:one)
    u.update(ip_restriction: restriction.join(", "))
    assert_equal(restriction, u.ip_restriction)
    u.update(ip_restriction: restriction.join("|,"))
    assert_equal(restriction, u.ip_restriction)
  end

  test "tracks create event" do
    u = User.new(email: "a@murb.nl", password: "asdfasdf", password_confirmation: "asdfasdf")
    u.save
    assert_equal(:create, u.events.first.name)
    assert_equal(:create, u.events.last.name)
    assert(u.events.last.persisted?)
  end

  test "tracks sign_in" do
    u = users(:one)
    u.update(last_sign_in_at: Time.current, current_sign_in_at: Time.current, last_sign_in_ip: "123.123.123.123", current_sign_in_ip: "123.123.123.142", sign_in_count: 12)
    assert_equal(:sign_in, u.events.last.name)
    assert_equal("123.123.123.142", u.events.last.ip_address)
    assert(u.events.last.persisted?)
  end

  test "tracks password update" do
    u = users(:one)
    u.update(password: "asdfasdf1", password_confirmation: "asdfasdf1", current_sign_in_ip: "123.123.123.142")
    assert_equal(:password_update, u.events.last.name)
    assert_equal("123.123.123.142", u.events.last.ip_address)
    assert(u.events.last.persisted?)
  end

  test "tracks password reset request" do
    u = users(:one)
    u.current_sign_in_ip = "123.123.123.144"
    u.send_reset_password_instructions
    assert_equal(:sent_reset_password_instructions, u.events.last.name)
    assert_equal("123.123.123.144", u.events.last.ip_address)
    assert(u.events.last.persisted?)
  end

  test "tracks confirmation mail sent" do
    u = users(:one)
    u.current_sign_in_ip = "123.123.123.144"
    u.send_confirmation_instructions
    assert_equal(:sent_confirmation_instructions, u.events.last.name)
    assert_equal("123.123.123.144", u.events.last.ip_address)
    assert(u.events.last.persisted?)
  end

  test "tracks name update" do
    u = users(:one)
    u.name = "Something else"
    u.save
    assert_equal(:name_change, u.events.last.name)
    assert(u.events.last.persisted?)
  end

  test "tracks email update request" do
    u = users(:one)
    u.email = "verynewemail@murb.nl"
    u.save
    assert_equal(%w[email_change_request sent_confirmation_instructions], u.events.last(2).pluck(:name))
    assert(u.events.last.persisted?)
  end
  test "tracks email confirmed" do
    u = users(:one)
    u.update_columns(unconfirmed_email: "verynewemail@murb.nl")
    u.confirm
    assert_equal(:email_change, u.events.last.name)
    assert(u.events.last.persisted?)
  end
end
