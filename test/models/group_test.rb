require "test_helper"

class GroupTest < ActiveSupport::TestCase
  test "#ip_restriction ip restriction is empty by default" do
    g = Group.create(name: "test")
    assert_equal([], g.ip_restriction)
  end

  test "#ip_restriction accepts string" do
    restriction = %w[1.2.3.4 2.3.4.5]

    g = Group.create(name: "test", ip_restriction: restriction.join(","))
    assert_equal(restriction, g.ip_restriction)
  end
end
