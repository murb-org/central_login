require "test_helper"

class GroupsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @group = groups(:one)
  end

  test "anonymous should not get index" do
    get groups_url
    assert_redirected_to new_user_session_path
  end

  test "anonymous should not  get new" do
    get new_group_url
    assert_redirected_to new_user_session_path
  end

  test "anonymous should not  create group" do
    assert_no_difference("Group.count") do
      post groups_url, params: {group: {name: @group.name, resources: @group.resources, roles: @group.roles}}
    end

    assert_redirected_to new_user_session_path

    # assert_redirected_to group_url(Group.last)
  end

  test "anonymous should not show group" do
    get group_url(@group)
    assert_redirected_to new_user_session_path
  end

  test "anonymous should not get edit" do
    get edit_group_url(@group)
    assert_redirected_to new_user_session_path
  end

  test "anonymous should not update group" do
    patch group_url(@group), params: {group: {name: @group.name, resources: @group.resources, roles: @group.roles}}
    # assert_redirected_to group_url(@group)
    assert_redirected_to new_user_session_path
  end

  test "anonymous should not destroy group" do
    assert_difference("Group.count", 0) do
      delete group_url(@group)
    end

    # assert_redirected_to groups_url
    assert_redirected_to new_user_session_path
  end

  test "user should not get index" do
    sign_in users(:one)
    get groups_url
    assert_response :not_found
  end

  test "admin should get index" do
    sign_in users(:admin)
    get groups_url
    assert_response :success
  end

  test "admin should get new" do
    sign_in users(:admin)
    get new_group_url
    assert_response :success
  end

  test "admin should create group" do
    sign_in users(:admin)
    assert_difference("Group.count") do
      post groups_url, params: {group: {name: @group.name, resources: @group.resources, roles: @group.roles}}
    end

    assert_redirected_to group_url(Group.last)
  end

  test "admin should show group" do
    sign_in users(:admin)
    get group_url(@group)
    assert_response :success
  end

  test "admin should get edit" do
    sign_in users(:admin)
    get edit_group_url(@group)
    assert_response :success
  end

  test "admin should update group" do
    sign_in users(:admin)
    patch group_url(@group), params: {group: {name: "new name", resources: @group.resources, roles: @group.roles}}
    assert_redirected_to group_url(@group)
    @group.reload
    assert_equal "new name", @group.name
  end

  test "admin should get errors when passing falty data" do
    sign_in users(:admin)
    old_group_name = @group.name
    patch group_url(@group), params: {group: {name: nil, resources: @group.resources, roles: @group.roles}}
    assert_response :unprocessable_entity
    @group.reload
    assert_equal old_group_name, @group.name
  end

  test "admin should destroy group" do
    sign_in users(:admin)
    assert_difference("Group.count", -1) do
      delete group_url(@group)
    end

    assert_redirected_to groups_url
  end
end
