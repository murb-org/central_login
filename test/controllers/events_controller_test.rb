require "test_helper"

class EventsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "should redirect for anonymous user" do
    get events_url
    assert_redirected_to new_user_session_url
  end

  test "should reject non-admin user" do
    sign_in users(:one)
    get events_url
    assert_response :not_found
  end

  test "should accept admin user" do
    sign_in users(:admin)
    get events_url
    assert_response :success
    assert_match(/Events/, response.body)
    assert_match(/one@example.com/, response.body)
  end
end
