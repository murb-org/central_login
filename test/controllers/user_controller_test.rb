require "test_helper"

class UserControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "anonymous should not get index" do
    get users_url
    assert_redirected_to new_user_session_path
  end

  test "admin should get index" do
    sign_in users(:admin)
    get users_url
    assert_response :success
  end

  test "anonymous should not get edit" do
    get edit_user_url(users(:one).uuid)
    assert_redirected_to new_user_session_path
  end

  test "regular user should not edit" do
    sign_in users(:one)
    # assert_raise(ActiveRecord::RecordNotFound) do
    get users_url
    # end

    assert_equal("404", response.code)
  end

  test "admin should be able to edit groups" do
    sign_in users(:admin)

    assert_equal([], users(:one).groups)

    get edit_user_url(users(:one).uuid)
    patch user_url(users(:one).uuid), params: {user: {
      group_ids: [groups(:one).id]
    }}

    assert_equal([groups(:one)], users(:one).reload.groups)
  end

  test "admin should be able to send confirmation instructions" do
    sign_in users(:admin)

    assert_difference("Event.count", 2) do
      post user_reset_confirmation_path(users(:one).uuid)
    end

    assert users(:one).events.last(2).pluck(:name).include?("sent_confirmation_instructions")
  end

  test "admin should be able to send reset password instructions" do
    sign_in users(:admin)

    assert_difference("Event.count", 2) do
      post user_reset_password_path(users(:one).uuid)
    end

    assert users(:one).events.last(2).pluck(:name).include?("sent_reset_password_instructions")
  end

  test "admin should be able to destroy a user" do
    sign_in users(:admin)

    assert_difference("User.count", -1) do
      delete user_url(users(:one).uuid)
    end
  end
end
