class UsersController < ApplicationController
  before_action :authenticate_user_admin!
  before_action :set_user, only: [:update, :edit, :destroy, :reset_password, :reset_confirmation]

  def index
    @users = User.order(:email).all
  end

  def edit
  end

  def update
    if @user.update(user_params.merge(performed_by_user: current_user))
      redirect_to users_path, notice: "#{@user.email} has been succesfully updated"
    else
      render :edit
    end
  end

  def reset_password
    @token = @user.send_reset_password_instructions
    @resource = @user
  end

  def reset_confirmation
    @user.send_confirmation_instructions
    @token = @user.confirmation_token
    @resource = @user
  end

  def destroy
    if @user.destroy
      redirect_to users_path, notice: "#{@user.email} has been succesfully removed"
    else
      redirect_to user_path(@user), notice: "Removing failed"
    end
  end

  private

  def set_user
    @user = User.find_by_uuid(params[:user_id] || params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:name, :email, :user_admin, :ip_restriction, group_ids: [])
  end
end
