class ApplicationController < ActionController::Base
  before_action :set_locale
  before_action :authenticate_user!
  def home
  end

  private

  def authenticate_admin!
    authenticate_user!

    raise ActiveRecord::RecordNotFound, "Admin required" unless current_user.admin?
  end

  def authenticate_user_admin!
    authenticate_user!

    raise ActiveRecord::RecordNotFound, "User admin required" unless current_user.admin? || current_user.user_admin?
  end

  def set_locale
    if I18n.available_locales.any? { |loc| loc.to_s == extract_locale_from_accept_language_header }
      I18n.locale = extract_locale_from_accept_language_header
    end
  end

  def extract_locale_from_accept_language_header
    httpacc = request.env["HTTP_ACCEPT_LANGUAGE"]
    httpacc.scan(/^[a-z]{2}/).first unless httpacc.blank?
  end
end
