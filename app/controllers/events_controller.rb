class EventsController < ApplicationController
  before_action :authenticate_admin!

  def index
    user_id = if params[:user_id]
      User.where(uuid: params[:user_id]).pluck(:id).first
    end
    @events = Event
    @events = @events.where(user_id: user_id) if user_id
    @events = @events.limit(500).offset(offset)
    # @next = events_path(offset: offset + 250)
  end

  private

  # @return [Integer]
  def offset
    @offset ||= params[:offset].to_i
  end
end
