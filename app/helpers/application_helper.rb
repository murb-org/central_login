module ApplicationHelper
  def menu_link_to(desc, path, options = {})
    test_path = path.include?("//") ? path.sub("//", "").split("/")[1..1000].join("/") : path
    class_name = if options[:only_exact_path_match]
      (request.path.to_s == test_path.to_s) ? "active" : ""
    else
      request.path.to_s.starts_with?(test_path.to_s) ? "active" : ""
    end
    link = link_to desc.to_s, path, class: class_name
    if options[:wrap]
      sanitize "<li>#{link}</li>"
    else
      link
    end
  end
end
