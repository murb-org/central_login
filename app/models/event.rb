class Event < ApplicationRecord
  # NAMES = [:create, :sign_in, :password_reset, :femail_update]

  # validates :name, inclusion: {in: NAMES}
  belongs_to :user
  belongs_to :performed_by_user, optional: true, class_name: "User"

  def name
    read_attribute(:name)&.to_sym
  end
end
