class OauthApplications < ApplicationRecord
  def url
    protocol, white, domain, _rest = redirect_uri.split("\n")[0].split("/")
    [protocol, white, domain].join("/")
  end
end
