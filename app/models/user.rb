class User < ApplicationRecord
  devise :two_factor_authenticatable, otp_secret_encryption_key: Rails.application.credentials.otp_secret_encryption_key
  devise :registerable, :timeoutable, :recoverable, :rememberable, :validatable, :trackable, :confirmable, :lockable

  after_create :track_create_event
  before_update :track_events
  attr_accessor :performed_by_user

  has_many :access_grants,
    class_name: "Doorkeeper::AccessGrant",
    foreign_key: :resource_owner_id,
    dependent: :destroy

  has_many :access_tokens,
    class_name: "Doorkeeper::AccessToken",
    foreign_key: :resource_owner_id,
    dependent: :destroy

  has_many :events

  has_and_belongs_to_many :groups

  before_save :set_uuid

  alias_method :email_verified?, :confirmed? # open_id semantics

  def some_kind_of_admin?
    admin? || user_admin?
  end

  def roles_as_array
    groups.flat_map { |g| g.roles_as_array }
  end

  def resources_as_array
    groups.flat_map { |g| g.resources_as_array }
  end

  def to_param
    uuid
  end

  def adheres_to_ip_restriction?(ip)
    ip_restriction.empty? || ip_restriction.include?(ip)
  end

  def ip_restriction= value
    if value.is_a? String
      super(value.split(/[,|\s]/).select(&:present?))
    else
      super
    end
  end

  def ip_restriction
    read_attribute(:ip_restriction).present? ? read_attribute(:ip_restriction) : groups.flat_map { |g| g.ip_restriction }.compact
  end

  private

  def set_uuid
    self.uuid ||= SecureRandom.uuid
  end

  def track_create_event
    events.create(name: :create, ip_address: current_sign_in_ip)
  end

  def track_event_attributes(name:)
    {name: name, ip_address: performed_by_user&.current_sign_in_ip || current_sign_in_ip, performed_by_user: performed_by_user}
  end

  def track_events
    if changes["email"]
      events.create(track_event_attributes(name: :email_change))
    elsif changes["unconfirmed_email"]&.[](1)
      events.create(track_event_attributes(name: :email_change_request))
      if changes["confirmation_sent_at"]
        events.create(track_event_attributes(name: :sent_confirmation_instructions))
      end
    elsif changes["current_sign_in_at"]
      events.create(track_event_attributes(name: :sign_in))
      if changes["confirmation_sent_at"]
        events.create(track_event_attributes(name: :sent_confirmation_instructions))
      end
    elsif changes["encrypted_password"]
      events.create(track_event_attributes(name: :password_update))
    elsif changes["reset_password_sent_at"]
      events.create(track_event_attributes(name: :sent_reset_password_instructions))
    elsif changes["confirmation_sent_at"]
      events.create(track_event_attributes(name: :sent_confirmation_instructions))
    elsif changes["name"]
      events.create(track_event_attributes(name: :name_change))
    elsif changes["ip_restriction"]
      events.create(track_event_attributes(name: :ip_restriction_updated))
    elsif changes["confirmed_at"]
      events.create(track_event_attributes(name: :email_confirmed))
    elsif changes.empty?
      # noop
    elsif Rails.env.test? || Rails.env.development?
      raise "Unknown change: #{changes.inspect}"
    else
      events.create(track_event_attributes(name: :update))
    end
  end
end

# After each sign in, verify if the user is allowed to sign in from the current IP address
Warden::Manager.after_set_user except: :fetch do |record, warden, options|
  if record.respond_to?(:adheres_to_ip_restriction?) && warden.authenticated?(options[:scope])
    unless record.adheres_to_ip_restriction?(warden.request.remote_ip)
      scope = options[:scope]
      warden.logout(scope)
      throw :warden, scope: scope, message: "Not allowed"
    end
  end
end
