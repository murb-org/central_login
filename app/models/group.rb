class Group < ApplicationRecord
  validates_presence_of :name
  has_and_belongs_to_many :users

  def roles_as_array
    roles.split(/[\s\n,;]/).select(&:present?)
  end

  def resources_as_array
    resources.split(/[\s\n,;]/).select(&:present?)
  end

  def ip_restriction= value
    if value.is_a? String
      super(value.split(/[,|\s]/).select(&:present?))
    else
      super(value)
    end
  end

  def ip_restriction
    read_attribute(:ip_restriction).present? ? read_attribute(:ip_restriction) : []
  end
end
