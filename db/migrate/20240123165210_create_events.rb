class CreateEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :events do |t|
      t.string :name
      t.string :details
      t.integer :user_id
      t.string :ip_address

      t.timestamps
    end
  end
end
