class AddIpRestrictionToUsers < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :ip_restriction, :json
    add_column :groups, :ip_restriction, :json
  end
end
