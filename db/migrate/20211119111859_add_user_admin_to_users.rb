class AddUserAdminToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :user_admin, :boolean
  end
end
