class AddSigninUrlToOauthApplications < ActiveRecord::Migration[6.1]
  def change
    add_column :oauth_applications, :signin_url, :string
    add_column :oauth_applications, :description, :text
    add_column :oauth_applications, :homepage_url, :string
  end
end
