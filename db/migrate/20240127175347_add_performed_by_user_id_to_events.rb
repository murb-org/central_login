class AddPerformedByUserIdToEvents < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :performed_by_user_id, :integer
  end
end
