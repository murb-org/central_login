class CreateGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :groups do |t|
      t.string :name
      t.text :roles
      t.text :resources

      t.timestamps
    end

    create_table "groups_users", id: false, force: :cascade do |t|
      t.integer "user_id", null: false
      t.integer "group_id", null: false
      t.index ["group_id"], name: "index_groups_users_on_group_id"
      t.index ["user_id"], name: "index_groups_users_on_user_id"
    end
  end
end
